package strategy;

import org.junit.jupiter.api.Test;
import player.PlayerStatus;

import static org.junit.jupiter.api.Assertions.*;

class DefaultTest {
    @Test
    void BelowStickyPointTest() {
        Default defaultNew = new Default();

        PlayerStatus result = defaultNew.implement(12);

        assertEquals(PlayerStatus.HIT, result);

    }

    @Test
    void StickyTest() {
        Default defaultNew = new Default();

        PlayerStatus result = defaultNew.implement(17);

        assertEquals(PlayerStatus.STICK, result);

    }

    @Test
    void MaxPointTest() {
        Default defaultNew = new Default();

        PlayerStatus result = defaultNew.implement(21);

        assertEquals(PlayerStatus.STICK, result);

    }

    @Test
    void AboveMaxPointTest() {
        Default defaultNew = new Default();

        PlayerStatus result = defaultNew.implement(25);

        assertEquals(PlayerStatus.BUST, result);

    }

}