package strategy;

import org.junit.jupiter.api.Test;
import player.PlayerStatus;

import static org.junit.jupiter.api.Assertions.*;

class AlwaysStickTest {
    @Test
    void BelowMaxPointTest() {
        AlwaysStick alwaysStick = new AlwaysStick();

        PlayerStatus result = alwaysStick.implement(12);

        assertEquals(PlayerStatus.STICK, result);

    }

    @Test
    void MaxPointTest() {
        AlwaysStick alwaysStick = new AlwaysStick();

        PlayerStatus result = alwaysStick.implement(21);

        assertEquals(PlayerStatus.STICK, result);

    }

    @Test
    void AboveMaxPointTest() {
        AlwaysStick alwaysStick = new AlwaysStick();

        PlayerStatus result = alwaysStick.implement(25);

        assertEquals(PlayerStatus.BUST, result);

    }

}