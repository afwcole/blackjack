package strategy;

import org.junit.jupiter.api.Test;
import player.PlayerStatus;

import static org.junit.jupiter.api.Assertions.*;

class AlwaysHitTest {

    @Test
    void BelowMaxPointTest() {
        AlwaysHit alwaysHit = new AlwaysHit();

        PlayerStatus result = alwaysHit.implement(12);

        assertEquals(PlayerStatus.HIT, result);

    }

    @Test
    void MaxPointTest() {
        AlwaysHit alwaysHit = new AlwaysHit();

        PlayerStatus result = alwaysHit.implement(21);

        assertEquals(PlayerStatus.STICK, result);

    }

    @Test
    void AboveMaxPointTest() {
        AlwaysHit alwaysHit = new AlwaysHit();

        PlayerStatus result = alwaysHit.implement(25);

        assertEquals(PlayerStatus.BUST, result);

    }
}