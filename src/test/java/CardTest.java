import card.Card;
import card.CardValue;
import card.Suit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {

    @Test
    void getGameValueTest() {
        Card testCard = new Card(Suit.DIAMONDS, CardValue.TWO);

        assertEquals(2, testCard.getGameValue());
    }
}