//import card.Card;
//import card.CardValue;
//import card.Suit;
//import game.Game;
//import game.InvalidArgumentException;
//import org.junit.jupiter.api.Test;
//import player.Player;
//
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//class GameTest {
//
//    @Test
//    void dealAllActivePlayers() {
//        try{
//            Game newGame = new Game(3);
//            newGame.dealAllActivePlayers();
//
//            List<Player> activePlayers = newGame.getActivePlayers();
//
//            assertEquals(2, activePlayers.get(0).getCardsInHand().size());
//            assertEquals(2, activePlayers.get(1).getCardsInHand().size());
//            assertEquals(2, activePlayers.get(2).getCardsInHand().size());
//        }catch(InvalidArgumentException e){
//            System.err.println(e.getMessage());
//        }
//
//    }
//
//    @Test
//    void playExtraRound() {
//        try{
//            Game newGame = new Game(3);
//            newGame.dealAllActivePlayers();
//            List<Player> activePlayers = newGame.getActivePlayers();
//        }catch(InvalidArgumentException e){
//            System.err.println(e.getMessage());
//        }
//    }
//
//    @Test
//    void checkWinner() {
//        try{
//            Game anotherGame = new Game(3);
//            List<Player> newPlayers = anotherGame.getActivePlayers();
//
//            Card newCard = new Card(Suit.SPADES, CardValue.ACE);
//            Card anotherCard = new Card(Suit.SPADES, CardValue.SIX);
//            Card testCard = new Card(Suit.CLUBS, CardValue.NINE);
//            Card extraCard = new Card(Suit.CLUBS, CardValue.ACE);
//
//            newPlayers.get(0).addToHand(List.of(newCard, testCard)); // stick
//            newPlayers.get(1).addToHand(List.of(newCard, anotherCard)); // stick
//            newPlayers.get(2).addToHand(List.of(newCard, extraCard)); // bust
//
//
//            assertEquals(List.of(newPlayers.get(0)), anotherGame.checkWinner());
//        }catch(InvalidArgumentException e){
//            System.err.println(e.getMessage());
//        }
//    }
//}