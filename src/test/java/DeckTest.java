import card.Deck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckTest {

    @Test
    void getCardsTest() {
        Deck testDeck = new Deck();

        assertEquals(2, testDeck.getCards(2).size());
    }

    @Test
    void getCardTest() {
        Deck testDeck = new Deck();

        assertEquals(1, testDeck.getCards(1).size());
    }

    @Test
    void deckSizeTest(){
        Deck testDeck = new Deck();

        assertEquals(52, testDeck.getSize());
    }

    @Test
    void deckSizeReducesOnCardRemoveTest(){
        Deck testDeck = new Deck();

        testDeck.getCards(1);

        assertEquals(51, testDeck.getSize());
    }
}