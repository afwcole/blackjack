import card.Card;
import card.CardValue;
import card.Suit;
import org.junit.jupiter.api.Test;
import player.Player;
import player.PlayerStatus;
import strategy.Default;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

    @Test
    void getPointsTest() {
        Player newPlayer = new Player(1, new Default());
        Card newCard = new Card(Suit.SPADES, CardValue.TWO);
        newPlayer.addToHand(List.of(newCard));

        assertEquals(2, newPlayer.getPoints());

    }

    @Test
    void addToHandTest() {
        Player newPlayer = new Player(1, new Default());
        Card newCard = new Card(Suit.SPADES, CardValue.TWO);
        newPlayer.addToHand(List.of(newCard));

        assertEquals(1, newPlayer.getCardsInHand().size());

    }

    @Test
    void getCardsInHandTest() {
        Player newPlayer = new Player(1, new Default());
        Card newCard = new Card(Suit.SPADES, CardValue.TWO);
        newPlayer.addToHand(List.of(newCard));

        assertIterableEquals(List.of(newCard), newPlayer.getCardsInHand());
    }

    @Test
    void changeStatusToHit(){
        Player newPlayer = new Player(1, new Default());
        Card newCard = new Card(Suit.SPADES, CardValue.TWO);
        newPlayer.addToHand(List.of(newCard));


        assertEquals(PlayerStatus.HIT, newPlayer.getStatus());

    }

    @Test
    void changeStatusToStick(){
        Player newPlayer = new Player(1, new Default());
        Card newCard = new Card(Suit.SPADES, CardValue.ACE);
        Card anotherCard = new Card(Suit.SPADES, CardValue.SIX);
        newPlayer.addToHand(List.of(newCard, anotherCard));


        assertEquals(PlayerStatus.STICK, newPlayer.getStatus());
    }

    @Test
    void changeStatusToBust(){
        Player newPlayer = new Player(1, new Default());
        Card newCard = new Card(Suit.SPADES, CardValue.ACE);
        Card anotherCard = new Card(Suit.SPADES, CardValue.ACE);
        newPlayer.addToHand(List.of(newCard, anotherCard));


        assertEquals(PlayerStatus.BUST, newPlayer.getStatus());
    }
}