package card;

public class Card {
    private final Suit suit;
    private final CardValue value;

    public Card(Suit suit, CardValue value){
        this.suit = suit;
        this.value = value;
    }

    @Override
    public String toString() {
        return "(" + value.getGameValue() + ")" +
                " " + value.name() + " OF " + suit.name();
    }

    public int getGameValue(){
        return value.getGameValue();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card that = (Card) o;
        return suit == that.suit && value == that.value;
    }
}
