package card;

import java.util.*;

public class Deck {
    private ArrayList<Card> cardDeck = new ArrayList<>();

    public Deck(){
        createDeck();
        shuffleDeck();
    }

    public int getSize(){
        return cardDeck.size();
    }

    public List<Card> getCards(int numberOfCards){
        List<Card> dealtCards = new ArrayList<>();
        int length = cardDeck.size();
        for (int i = length - 1; i >= length - numberOfCards; --i){
            dealtCards.add(cardDeck.remove(i));
        }
        return dealtCards;
    }

    private void createDeck(){

        for (Suit suit : Suit.values()){
            for (CardValue value : CardValue.values()){
                cardDeck.add(new Card(suit, value));
            }
        }

    }

    public void shuffleDeck(){
        Collections.shuffle(cardDeck);
    }
}
