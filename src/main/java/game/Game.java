package game;

import card.Deck;
import player.Player;
import player.PlayerFactory;
import player.PlayerStatus;
import shuffle.Shuffle;
import strategy.Strategy;

import java.util.ArrayList;
import java.util.List;


public class Game {

    private List<Player> activePlayers;
    private Deck deck = new Deck();
    private final int WINNING_POINT = 21;

    private Shuffle shuffle;

    public Game(String[] args) throws InvalidArgumentException {
        List<String> strategies = processArgs(args);
        activePlayers = createPlayers(strategies);
    }

    private List<String> processArgs(String[] args) throws InvalidArgumentException{
        List<String> strategies = new ArrayList<>();
        int i = 0;
        while (i < args.length){
            String arg = args[i];
            if (arg.equals("--shuffle")){
                //Set next array element as shuffle
                deck.shuffleDeck();
                //increment the index by 1
                ++i;
            } else if(arg.equals("--player")){
                if (i + 1 < args.length){
                    strategies.add(args[i + 1]);
                } else {
                    throw new InvalidArgumentException("Did not set a Strategy for a player");
                }
                ++i;
            }
            ++i;
        }

        return strategies;
    }

    public void dealAllActivePlayers(){
        for(Player player: activePlayers){
            dealCard(player, 2);
        }
    }

    public void playExtraRound(){
        List<Player> playersToRemove = new ArrayList<>();
        for (int i = 0; i < activePlayers.size(); ++i){
            Player player = activePlayers.get(i);
            System.out.println("\n----- Player " + player.getPlayerID() + " -----");
            player.printHand();
            if(player.getStatus() == PlayerStatus.HIT){
                dealCard(player);
            }else if (player.getStatus() == PlayerStatus.BUST){
                playersToRemove.add(player);
            }
        }

        printPlayersRemoved(playersToRemove);
        activePlayers.removeAll(playersToRemove);
    }
    private List<Player> createPlayers(List<String> strategyArgs) throws InvalidArgumentException{
        return PlayerFactory.createPlayers(strategyArgs);
    }

    private void dealCard(Player player){
        dealCard(player, 1);
    }
    private void dealCard(Player player, int numberOfCards){
        player.addToHand(deck.getCards(numberOfCards));
    }

    private void removePlayer(Player player){
        System.out.println("\nPlayer " + player.getPlayerID() + " has been removed");
        activePlayers.remove(player);
    }

    private void printPlayersRemoved(List<Player> playersToRemove){
        System.out.println(" ");
        for (Player player : playersToRemove){
            System.out.println("❌ Player " + player.getPlayerID() + " was removed");
        }
    }


    public List<Player> checkWinner(){
        if(activePlayers.size() == 1){
            return List.of(activePlayers.get(0));
        }

        int difference = Integer.MAX_VALUE;
        List<Player> winners = new ArrayList<>();
        int numSticks = 0;
        for(Player player: activePlayers){
            if(player.getPoints() == WINNING_POINT){
                return List.of(player);
            } else if(player.getStatus() == PlayerStatus.STICK){
                ++numSticks;
                int currentDifference = WINNING_POINT - player.getPoints();
                if (currentDifference < difference){
                    difference = currentDifference;
                    winners.clear();
                    winners.add(player);
                }
                else if(currentDifference == difference){
                    winners.add(player);
                }
            }
        }

        return numSticks == activePlayers.size() ? winners : new ArrayList<>();
    }

    private void checkStatus(Player player){
        if(player.getStatus() == PlayerStatus.HIT){
            dealCard(player);
        }
        else if(player.getStatus() == PlayerStatus.BUST){
            removePlayer(player);
        }
    }

    public List<Player> getActivePlayers() {
        return activePlayers;
    }


}
