import game.Game;
import game.InvalidArgumentException;
import player.Player;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {

        try {
            Game newGame = new Game(args);

            System.out.println("\n*******************************");
            System.out.println("  ♥♣NEW BLACKJACK GAME♠♦");
            System.out.println("*******************************");


            newGame.dealAllActivePlayers();

            List<Player> winners = new ArrayList<>();

            do {
                winners = newGame.checkWinner();
                newGame.playExtraRound();
                System.out.println("\n********************************");
            } while (winners.isEmpty());


            System.out.println("\n\n********** GAME ENDS ***********");
            if (!winners.isEmpty()) {
                for (Player winner : winners) {
                    System.out.println("\n\t🎉 Player " + winner.getPlayerID() + " wins!!! 🎉");
                }
            } else {
                System.out.println("\n\t😭 No Winner 😭");
            }
            System.out.println("\n********************************");
        } catch (InvalidArgumentException e){
            System.err.println(e.getMessage());
        }

    }
}
