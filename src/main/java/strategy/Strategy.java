package strategy;

import game.InvalidArgumentException;
import player.PlayerStatus;

public abstract class Strategy {

    public static final String ALWAYS_HIT = "always-hit";
    public static final String ALWAYS_STICK = "always-stick";
    public static final String DEFAULT = "default";
    protected final int MAX_POINTS = 21;
    public abstract PlayerStatus implement(int points);


    public static Strategy determineStrategy(String strategyArg) throws InvalidArgumentException{
        switch(strategyArg){
            case ALWAYS_HIT:
                return new AlwaysHit();
            case ALWAYS_STICK:
                return new AlwaysStick();
            case DEFAULT:
                return new Default();
            default:
                throw new InvalidArgumentException("Invalid strategy argument.");
        }
    }
}
