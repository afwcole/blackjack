package strategy;

import player.PlayerStatus;

public class AlwaysHit extends Strategy{
    @Override
    public PlayerStatus implement(int points) {
        if (points < 21){
            return PlayerStatus.HIT;
        }
        else if(points == MAX_POINTS){
            return PlayerStatus.STICK;
        }
        else {
            return PlayerStatus.BUST;
        }
    }
}
