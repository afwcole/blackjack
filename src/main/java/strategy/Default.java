package strategy;

import player.PlayerStatus;

public class Default extends Strategy{
    private final int STICKY_POINT = 17;
    @Override
    public PlayerStatus implement(int points) {
        if(points < STICKY_POINT){
            return PlayerStatus.HIT;
        }
        else if(points <= MAX_POINTS){
            return PlayerStatus.STICK;
        } else {
            return PlayerStatus.BUST;
        }
    }
}
