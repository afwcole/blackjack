package strategy;

import player.PlayerStatus;

public class AlwaysStick extends Strategy{
    @Override
    public PlayerStatus implement(int points) {
        if (points <= MAX_POINTS){
            return PlayerStatus.STICK;
        }
        else {
            return PlayerStatus.BUST;
        }
    }
}
