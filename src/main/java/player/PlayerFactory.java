package player;

import game.InvalidArgumentException;
import strategy.Default;
import strategy.Strategy;

import java.util.ArrayList;
import java.util.List;

public abstract class PlayerFactory {
    public static List<Player> createdPlayers = new ArrayList<>();

    public static List<Player> createPlayers(List<String> strategyArgs) throws InvalidArgumentException {
        for(int i=0; i < strategyArgs.size(); ++i){
            Player newPlayer = new Player(i + 1, Strategy.determineStrategy(strategyArgs.get(i)));
            createdPlayers.add(newPlayer);
        }
        return createdPlayers;
    }
}
