package player;

import card.Card;
import strategy.Strategy;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private int points = 0;
    private List<Card> cardsInHand = new ArrayList<>();
    private PlayerStatus status;
    private int playerID;
    private Strategy strategy;


    public Player(int ID, Strategy strategy){
        playerID = ID;
        this.strategy = strategy;
    }

    public Player(){
        playerID = (int)(Math.random() * 10);
    }



    public int getPlayerID() { return playerID; }

    public PlayerStatus getStatus() {
        return status;
    }
    public int getPoints() {
        return points;
    }
    public List<Card> getCardsInHand() {
        return cardsInHand;
    }


    private void addPoint(Card card){
        points += card.getGameValue();
    }

    public void addToHand(List<Card> dealtCards){
        for(Card dealtCard : dealtCards){
            cardsInHand.add(dealtCard);
            addPoint(dealtCard);
            updateStatus();
        }
    }

    private void updateStatus(){
        status = strategy.implement(points);
    }

    public void printHand(){
        for(Card card: cardsInHand){
            System.out.println(card.toString());
        }
    }

}
